# packer puglin for AWS 
# https://www.packer.io/plugins/builders/amazon 
packer {
  required_plugins {
    amazon = {
      version = ">= 1.3.0"
      source  = "github.com/hashicorp/amazon"
    }
  }
}

# which ami to use as the base and where to save it
source "amazon-ebs" "ubuntu" {
  region          = "us-east-1"
  ami_name        = "ami-ubuntu-{{timestamp}}"
  instance_type   = "t2.micro"
  source_ami      = "ami-0c7217cdde317cfec"
  ssh_username    = "ubuntu"
  ami_users       = ["293917936618"]
  ami_regions     = [
                      "us-east-1",
                    
                     
                    ]
}

# what to install, configure and file to copy/execute
build {
  name = "hq-packer"
  sources = [
    "source.amazon-ebs.ubuntu"
  ]

  provisioner "file" {
  source = "provisioner.sh"
  destination = "/tmp/provisioner.sh"
}

# https://www.packer.io/docs/provisioners/shell 
provisioner "shell" {
    inline = ["chmod a+x /tmp/provisioner.sh"]
  }

  provisioner "shell" {
    inline = ["/tmp/provisioner.sh"]
  }
}
